# Tyūryoku | 注力

[TOC]

![capture](img/capture.png)

## Japanese | 日本語

Tyūryoku（注力）は、個人的な用途のために開発したフォーカス維持ソフトウェアです。マウスの左クリックを離すと、事前に設定したウィンドウに自動的にフォーカスが移ります。ただし、フォーカス移動は即時ではなく、100ミリ秒後に行われます。

### フォーカスの設定

1. `tyuryoku.exe`を実行する
2. フォーカスしたいウィンドウを選択する
3. `Active` チェックボックスをオンにする

フォーカス機能が正常に動作しない場合は、exeファイルを管理者権限で実行してみてください。

## English | 英語

Tyūryoku (注力) is a software developed for personal use to help maintain focus. It automatically shifts focus to a pre-set window when you release the mouse's left click. However, the focus shift is not immediate and occurs 100 milliseconds later.

### Setting the focus

1. Run `tyuryoku.exe`
2. Select the window you want to focus on
3. Turn on the `Active` checkbox

If the focus function does not operate correctly, try running the exe file with administrator privileges.

## License | ライセンス

See [LICENSE](./LICENSE).
