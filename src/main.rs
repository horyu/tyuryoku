#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::{
    io::Write,
    sync::{
        atomic::{AtomicBool, Ordering},
        Mutex,
    },
    time::Duration,
};

use once_cell::sync::Lazy;
use windows::{
    core::*,
    Win32::{
        Foundation::*,
        Graphics::Gdi::{UpdateWindow, ValidateRect},
        System::{LibraryLoader::GetModuleHandleA, Threading::AttachThreadInput},
        UI::{Controls::*, WindowsAndMessaging::*},
    },
};

const RELOAD_BUTTON_ID: isize = 1;
const FOCUS_CHECKBOX_ID: isize = 2;
const LISTVIEW_ID: isize = 3;

static mut HWND_LISTVIEW: HWND = HWND(0);

static mut FOCUS_TARGET_HWND: Lazy<Mutex<HWND>> = Lazy::new(|| Mutex::new(HWND::default()));

static mut KEEP_FOCUS_IS_ACTIVE: AtomicBool = AtomicBool::new(false);

static mut KEEP_FOCUS_SHOULD_CHECK: AtomicBool = AtomicBool::new(false);

fn main() {
    if let Err(err) = try_main() {
        dbg!(&err);
        if let Ok(mut f) = std::fs::File::create("tyuryoku-error.log") {
            let _ = f.write_fmt(format_args!("{err:#?}"));
        }
        std::process::exit(1);
    }
}

fn try_main() -> Result<()> {
    unsafe {
        let instance = GetModuleHandleA(None)?;
        debug_assert!(instance.0 != 0);

        let h_hook =
            SetWindowsHookExA(WH_MOUSE_LL, Some(mouse_proc), HINSTANCE::from(instance), 0)?;

        let window_class = s!("tyuryoku");

        let wc = WNDCLASSA {
            hInstance: instance.into(),
            lpszClassName: window_class,
            lpfnWndProc: Some(wndproc),
            ..Default::default()
        };

        let atom = RegisterClassA(&wc);
        debug_assert!(atom != 0);

        init_windows(window_class, instance);

        std::thread::spawn(|| loop {
            std::thread::sleep(Duration::from_millis(100));
            focus_if_need()
        });

        let mut message = MSG::default();
        while GetMessageA(&mut message, HWND::default(), 0, 0).as_bool() {
            // TranslateMessage(&message);
            DispatchMessageA(&message);
        }

        // These processes are automatically performed by the OS when the process ends, so they are not necessary.
        UnhookWindowsHookEx(h_hook)?;
        UnregisterClassA(window_class, instance)?;
    }
    Ok(())
}

extern "system" fn wndproc(window: HWND, message: u32, wparam: WPARAM, lparam: LPARAM) -> LRESULT {
    unsafe {
        match message {
            WM_PAINT => {
                eprintln!("WM_PAINT");
                ValidateRect(window, None);
                LRESULT(0)
            }
            WM_DESTROY => {
                eprintln!("WM_DESTROY");
                PostQuitMessage(0);
                LRESULT(0)
            }
            WM_COMMAND => {
                match wparam.0 as isize {
                    RELOAD_BUTTON_ID => {
                        eprintln!("WM_COMMAND: RELOAD_BUTTON_ID");
                        debug_assert!(HWND_LISTVIEW != window);
                        dbg!(HWND_LISTVIEW, lparam);
                        // let hwnd = &*(lparam.0 as *const HWND);
                        // eprintln!("WM_COMMAND HWND: {hwnd:?}");
                        let hwnd = lparam.0;
                        eprintln!("WM_COMMAND HWND: {hwnd:?}");
                        reload_window_list();
                    }
                    FOCUS_CHECKBOX_ID => {
                        eprintln!("WM_COMMAND: FOCUS_CHECKBOX_ID");
                        if IsDlgButtonChecked(window, FOCUS_CHECKBOX_ID as i32) == BST_CHECKED.0 {
                            eprintln!("FOCUS_CHECKBOX_ID: Checked");
                            KEEP_FOCUS_IS_ACTIVE.store(true, Ordering::Relaxed);
                        } else {
                            eprintln!("FOCUS_CHECKBOX_ID: Unchecked");
                            *FOCUS_TARGET_HWND.lock().unwrap() = HWND::default();
                            KEEP_FOCUS_IS_ACTIVE.store(false, Ordering::Relaxed);
                        }
                    }
                    _ => {}
                }
                DefWindowProcA(window, message, wparam, lparam)
            }
            WM_NOTIFY => {
                let nmhdr = &*(lparam.0 as *const NMHDR);
                // eprintln!("{:?}", *nmhdr);
                if nmhdr.code == LVN_ITEMCHANGED {
                    let lvitem = lparam.0 as *const NMLISTVIEW;
                    let hwnd = HWND((*lvitem).lParam.0);
                    eprintln!("WM_NOTIFY: LVN_ITEMCHANGED: {hwnd:?}");
                    *FOCUS_TARGET_HWND.lock().unwrap() = hwnd;
                    KEEP_FOCUS_SHOULD_CHECK.store(true, Ordering::Relaxed);
                }
                DefWindowProcA(window, message, wparam, lparam)
            }
            _ => DefWindowProcA(window, message, wparam, lparam),
        }
    }
}

unsafe fn init_windows(window_class: PCSTR, instance: HMODULE) {
    let hwnd = CreateWindowExA(
        WINDOW_EX_STYLE::default(),
        window_class,
        s!("Tyuryoku"),
        WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        300,
        350,
        None,
        None,
        instance,
        None,
    );
    // dbg!(hwnd);
    debug_assert!(hwnd.0 != 0);
    UpdateWindow(hwnd);

    let init_common_controls_ex_result = InitCommonControlsEx(&INITCOMMONCONTROLSEX {
        dwSize: std::mem::size_of::<INITCOMMONCONTROLSEX>() as u32,
        dwICC: ICC_LISTVIEW_CLASSES,
    });
    dbg!(init_common_controls_ex_result.as_bool());

    // Add Button to reload window list
    let hwnd_button = CreateWindowExA(
        WINDOW_EX_STYLE::default(),
        WC_BUTTONA,
        s!("Reload"),
        WS_CHILD | WS_VISIBLE | WINDOW_STYLE(BS_PUSHBUTTON as u32),
        0,
        0,
        150,
        25,
        hwnd,
        HMENU(RELOAD_BUTTON_ID),
        HINSTANCE::from(instance),
        None,
    );
    // dbg!(hwnd_button);
    debug_assert!(hwnd_button.0 != 0);
    UpdateWindow(hwnd_button);

    // Add Checkbox with label
    let hwnd_checkbox = CreateWindowExA(
        WINDOW_EX_STYLE::default(),
        WC_BUTTONA,
        s!("Active"),
        WS_CHILD | WS_VISIBLE | WINDOW_STYLE((BS_AUTOCHECKBOX | BS_CENTER) as u32),
        150,
        0,
        150,
        25,
        hwnd,
        HMENU(FOCUS_CHECKBOX_ID),
        HINSTANCE::from(instance),
        None,
    );
    // dbg!(hwnd_checkbox);
    debug_assert!(hwnd_checkbox.0 != 0);
    UpdateWindow(hwnd_checkbox);

    // Add ListView
    HWND_LISTVIEW = CreateWindowExA(
        WINDOW_EX_STYLE::default(),
        WC_LISTVIEWA,
        s!("ListView"),
        WS_CHILD
            | WS_VISIBLE
            | WS_BORDER
            | WINDOW_STYLE(LVS_REPORT)
            | WINDOW_STYLE(LVS_NOCOLUMNHEADER)
            | WINDOW_STYLE(LVS_SINGLESEL),
        0,
        25,
        300,
        // To hide the horizontal scrollbar of the ListView, set it slightly larger.
        305,
        hwnd,
        HMENU(LISTVIEW_ID),
        HINSTANCE::from(instance),
        None,
    );
    // Include the left and right margins in the click area.
    SendMessageW(
        HWND_LISTVIEW,
        LVM_SETEXTENDEDLISTVIEWSTYLE,
        WPARAM(LVS_EX_FULLROWSELECT as usize),
        LPARAM(LVS_EX_FULLROWSELECT as isize),
    );

    let lvcolumn = LVCOLUMNW {
        mask: LVCF_TEXT | LVCF_WIDTH,
        pszText: PWSTR::null(),
        cx: 300,
        ..Default::default()
    };
    SendMessageW(
        HWND_LISTVIEW,
        LVM_INSERTCOLUMNW,
        WPARAM::default(),
        LPARAM(&lvcolumn as *const _ as isize),
    );

    reload_window_list();
}

unsafe fn reload_window_list() {
    // Clear the list view
    SendMessageW(
        HWND_LISTVIEW,
        LVM_DELETEALLITEMS,
        WPARAM::default(),
        LPARAM::default(),
    );

    let mut window_tilte_and_hwnd_list: Vec<(Vec<u16>, HWND)> = vec![];
    if let Err(e) = EnumWindows(
        Some(enum_window_proc),
        LPARAM(&mut window_tilte_and_hwnd_list as *mut _ as isize),
    ) {
        eprintln!("EnumWindows failed: {e:?}");
    }
    // Insert in reverse order to bring the latest window to the top
    for (title, hwnd) in window_tilte_and_hwnd_list.into_iter().rev() {
        // let title_string =
        //     String::from_utf16_lossy(&title[..title.iter().position(|&x| x == 0).unwrap()]);
        // eprintln!("HWND: {hwnd:?} {title_string}");
        let lvitem = LVITEMW {
            mask: LVIF_TEXT | LVIF_PARAM,
            iItem: 0,
            iSubItem: 0,
            pszText: PWSTR::from_raw(title.as_ptr() as *mut _),
            lParam: LPARAM(hwnd.0),
            ..Default::default()
        };
        SendMessageW(
            HWND_LISTVIEW,
            LVM_INSERTITEMW,
            WPARAM::default(),
            LPARAM(&lvitem as *const _ as isize),
        );
    }
    UpdateWindow(HWND_LISTVIEW);
}

/// https://wisdom.sakura.ne.jp/system/winapi/win32/win142.html
extern "system" fn enum_window_proc(hwnd: HWND, lparam: LPARAM) -> BOOL {
    let mut title = vec![0u16; 1024];
    unsafe {
        if GetWindow(hwnd, GW_OWNER) != HWND(0)
            || !IsWindowVisible(hwnd).as_bool()
            || GetWindowTextW(hwnd, &mut title) == 0
        {
            return TRUE;
        }
        let window_tilte_and_hwnd_list = &mut *(lparam.0 as *mut Vec<(Vec<u16>, HWND)>);
        window_tilte_and_hwnd_list.push((title, hwnd));
    }

    TRUE
}

extern "system" fn mouse_proc(ncode: i32, wparam: WPARAM, lparam: LPARAM) -> LRESULT {
    unsafe {
        if ncode >= 0 && wparam.0 as u32 == WM_LBUTTONUP {
            // The left mouse button was released.
            KEEP_FOCUS_SHOULD_CHECK.store(true, Ordering::Relaxed);
        }
        CallNextHookEx(HHOOK::default(), ncode, wparam, lparam)
    }
}

unsafe fn focus_if_need() {
    if !KEEP_FOCUS_IS_ACTIVE.load(Ordering::Relaxed) {
        return;
    }
    let hwnd = *FOCUS_TARGET_HWND.lock().unwrap();
    if hwnd == HWND::default() {
        return;
    }
    if KEEP_FOCUS_SHOULD_CHECK.swap(false, Ordering::Relaxed) {
        let is_success = force_active_window(hwnd);
        eprintln!("FOCUS: {is_success}");
    }
}

/// https://qiita.com/yaju/items/baaf8d243257cb5fbbca#%E6%81%92%E4%B9%85%E5%AF%BE%E5%BF%9C
unsafe fn force_active_window(handle: HWND) -> bool {
    for _ in 0..3 {
        let h_foreground_wnd = GetForegroundWindow();
        let dw_forground_id = GetWindowThreadProcessId(h_foreground_wnd, None);
        let dw_target_id = GetWindowThreadProcessId(handle, None);
        if dw_forground_id == dw_target_id {
            return true;
        }

        AttachThreadInput(dw_target_id, dw_forground_id, TRUE);

        let dw_timeout = None;
        let _ = SystemParametersInfoA(
            SPI_GETFOREGROUNDLOCKTIMEOUT,
            0,
            dw_timeout,
            Default::default(),
        );

        let _ = SystemParametersInfoA(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, None, SPIF_SENDCHANGE);

        let is_success = SetForegroundWindow(handle);

        let _ = SystemParametersInfoA(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, dw_timeout, SPIF_SENDCHANGE);

        AttachThreadInput(dw_target_id, dw_forground_id, FALSE);

        if is_success.as_bool() {
            return true;
        }
    }

    false
}
